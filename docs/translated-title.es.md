# Título en la navegación traducido

Esta página tiene un tíitulo configurado en nav y està traducido mediante la configuración del plugin i18n.

Vease <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L33> i <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L54>
